public class PrintSquare {
	public static void main(String[] args) {
		int s = 0;
		if (args.length != 1) {
			System.out.println("Try to enter one int");
		} else {
			try{
				s = Integer.parseInt(args[0]);
			} catch(Exception e) {
				System.out.println("Input must be int");
				System.exit(0);
			}
			if(s <= 0){
				System.out.println("Input must be a positive number");
			} else {
				char[][] table = new char[s][s];

				for (int i=0; i<s; i++){
					for (int j=0; j<s; j++){
						if (i == 0 || j == 0 ){
							table[i][j] = '#';
						}
						else if (i == s-1 || j == s-1){
							table[i][j] = '#';
						}
						else{
							table[i][j] = ' ';
						}
					}
				}
				for (int i = 0; i < table.length; i++){
					System.out.println(table[i]);
				}
			}
			
		}
	}
}